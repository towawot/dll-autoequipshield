#include <skse.h>
#include <skse/skse_version.h>
#include <skse/GameObjects.h>
#include <skse/GameRTTI.h>
// #include <skse/GameData.h>
#include <skse/GameEvents.h>
// #include <skse/GameExtraData.h>
// #include <skse/GameFormComponents.h>
#include <skse/GameForms.h>
#include <skse/GamePapyrus.h>
#include <skse/GameReferences.h>
#include <skse/PapyrusNativeFunctions.h>
#include <skse/PapyrusVM.h>
#include <skse/PluginAPI.h>
#include <skse/SafeWrite.h>

#include <shlobj.h>

// #include <set>

#if SKSE_VERSION_RELEASEIDX >= 40
// RUNTIME_VERSION_1_9_32_0
#define ADDR_IPlayerAnimationGraphManagerHolder_VPTR			0x010D1FB4
#elif SKSE_VERSION_RELEASEIDX == 39
// RUNTIME_VERSION_1_9_29_0
#error 対応していないバージョンです。
#elif SKSE_VERSION_RELEASEIDX == 38
#error 対応していないバージョンです。
#elif SKSE_VERSION_RELEASEIDX >= 31
// RUNTIME_VERSION_1_8_151_0
#define ADDR_IPlayerAnimationGraphManagerHolder_VPTR			0x010D196C
#else
#error 対応していないバージョンです。
#endif

TESForm* right = NULL;
TESForm* left  = NULL;

// --------------------------------------------------- Functions ---

const char* GetFullName(TESObjectREFR* ref)
{
	static const char unkName[] = "unknown";
	const char* result = unkName;
	TESFullName* pFullName = NULL;

	if (ref && ref->formType == kFormType_Character)
	{
		TESActorBase* actorBase = (TESActorBase*)ref->baseForm;
		if (actorBase)	pFullName = DYNAMIC_CAST(actorBase, TESActorBase, TESFullName);
	}
	else if (ref && ref->formType == kFormType_Reference)
	{
		TESForm* form = (TESForm*)ref->baseForm;
		if(form)	pFullName = DYNAMIC_CAST(form, TESForm, TESFullName);
	}

	if (pFullName)	result = pFullName->name.data;

	return result;
}

// --------------------------------------------------- Event ---

#include <string>
#include <algorithm>
static UInt32 fnIPlayerAnimationGraphManagerHolder_Unk_01;
static bool Log_IAnimationGraphManagerHolder_SendEvent(UInt32* stack, UInt32 ecx)
{
	BSFixedString* animName = (BSFixedString*)stack[1];
	TESObjectREFR* ref = (TESObjectREFR*)(ecx - 0x20);
	PlayerCharacter* player = *g_thePlayer;

	if (ref && ref->formType == kFormType_Character && (Actor*)ref == player)
	{
		std::string str = animName->data;
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);

		bool bStart = true;
		if (str == "weapequip" || str == "unequip")
		{
			if (str == "unequip")
			{
				bStart = false;
				right = NULL;
				left  = NULL;
			}
			else
			{
				right = player->GetEquippedObject(false);
				left  = player->GetEquippedObject(true);
			}
			static BSFixedString eventName("AEQ_OnWeaponEquip");
			PapyrusCall::SendEvent(ref, &eventName, right, left, bStart);
		}
		else if (str == "weapoutrightreplaceforceequip" || str == "weapoutleftreplaceforceequip")
		{
			UInt32 num = 0;
			TESForm* rightOld = right;
			TESForm* leftOld  = left;
			right = player->GetEquippedObject(false);
			left  = player->GetEquippedObject(true);
			if (leftOld != left)
			{
				num = 0;
				if (rightOld != right)
				{
					num = 2;
				}
			}
			if (rightOld != right)
			{
				num = 1;
			}
			else //if (leftOld == left && rightOld == right)
			{
				num = -1;
			}
			static BSFixedString eventName("AEQ_OnWeaponForceEquip");
			PapyrusCall::SendEvent(ref, &eventName, num, right, rightOld, left, leftOld);
		}
#ifdef _DEBUG
		const char* name = GetFullName(ref);
		_MESSAGE("%s: %s(%08X)", animName->data, name, ref);
#endif
	}
	return true;
}

static void __declspec(naked) Hook_IPlayerAnimationGraphManagerHolder_Unk_01(void)
{
	__asm
	{
		push	ecx
		lea		ecx, [esp+4]
		push	ecx
		call	Log_IAnimationGraphManagerHolder_SendEvent
		add		esp, 4
		pop		ecx
		
		test	al, al
		jnz		jmp_default

		retn	4
jmp_default:
		jmp		fnIPlayerAnimationGraphManagerHolder_Unk_01
	}
}

DECLARE_EVENT_HANDLER(MagicEffectApply, MagicEffectApplyEventHandler);
EventResult MagicEffectApplyEventHandler::ReceiveEvent(TESMagicEffectApplyEvent *evn, DispatcherT *dispatcher)
{
	// プレイヤーのAnimationGraphManagerHolderにフックをかける
	UInt32* vptr = (UInt32*)ADDR_IPlayerAnimationGraphManagerHolder_VPTR;
	fnIPlayerAnimationGraphManagerHolder_Unk_01 = vptr[0x01];
	SafeWrite32((UInt32)&vptr[0x01], (UInt32)&Hook_IPlayerAnimationGraphManagerHolder_Unk_01);

	Unregister();
	return kEvent_Continue;
}

#ifdef _DEBUG
	IDebugLog gLog;
#endif
PluginHandle g_pluginHandle = kPluginHandle_Invalid;

extern "C"
{

bool SKSEPlugin_Query(const SKSEInterface * skse, PluginInfo * info)
{

#ifdef _DEBUG
	gLog.OpenRelative(CSIDL_MYDOCUMENTS, "\\My Games\\Skyrim\\SKSE\\skse_towAutoEquipShield.log");
#endif
	// populate info structure
	info->infoVersion =	PluginInfo::kInfoVersion;
	info->name =		"towAutoEquipShield plugin";
	info->version =		1;
	
	// store plugin handle so we can identify ourselves later
	g_pluginHandle = skse->GetPluginHandle();
	
	if(skse->isEditor)
	{
#ifdef _DEBUG
		_MESSAGE("loaded in editor, marking as incompatible");
#endif
		return false;
	}

	int major = (skse->runtimeVersion >> 24) & 0xFF;
	int minor = (skse->runtimeVersion >> 16) & 0xFF;
	int build = (skse->runtimeVersion >> 8) & 0xFF;
	int sub   = skse->runtimeVersion & 0xFF;

	if(skse->runtimeVersion != SKSE_SUPPORTING_RUNTIME_VERSION)
	{
#ifdef _DEBUG
		_MESSAGE("unsupported runtime version 1.%d.%d.%d", major, minor, build);
#endif
		return false;
	}
#ifdef _DEBUG
 	_MESSAGE("skyrim runtime version: 1.%d.%d.%d ... OK !", major, minor, build);
#endif
	return true;
}

bool SKSEPlugin_Load(const SKSEInterface * skse)
{
#ifdef _DEBUG
 	_MESSAGE("load", g_pluginHandle);
#endif
	// イベントハンドラを登録
	static MagicEffectApplyEventHandler s_handlerMagicEffect;
	s_handlerMagicEffect.Register();
	
	return true;
}

};
